// Fill out your copyright notice in the Description page of Project Settings.


#include "AimingComponent.h"
#include "Characters/BaseCharacter.h"
#include "Engine/World.h"
#include "Weapons/Personal/PersonalWeapon.h"
#include "Camera/CameraComponent.h"
#include "DrawDebugHelpers.h"

// Sets default values for this component's properties
UAimingComponent::UAimingComponent()
{
	PrimaryComponentTick.bCanEverTick = true;
}


// Called when the game starts
void UAimingComponent::BeginPlay()
{
	Super::BeginPlay();

	if (!OwningCharacter)
	{
		UE_LOG(LogTemp, Error, TEXT("Aiming component does not have owner of type ABaseCharacter!"));
		return;
	}

	CameraComp = OwningCharacter->GetFirstPersonCameraComponent();

	if (!CameraComp)
	{
		UE_LOG(LogTemp, Error, TEXT("Aiming component's owner does not have a valid CameraComponent!"));
		return;
	}
}

// Called every frame
void UAimingComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	CurrentRecoil = CalculateRecoilRotator();
	
	FRotator DeltaRecoil = CurrentRecoil - PreviousRecoil;

	if (!OwningCharacter)
	{
		UE_LOG(LogTemp, Error, TEXT("Aiming component does not have owner of type ABaseCharacter!"));
		return;
	}

	OwningCharacter->Turn(DeltaRecoil.Yaw);
	OwningCharacter->LookUp(DeltaRecoil.Pitch);

	APersonalWeapon* Weapon = OwningCharacter->GetPrimaryWeapon();

	if (Weapon)
	{
		CurrentSpread = FMath::Clamp(CurrentSpread - CurrentSpreadDecreasePerSecond * DeltaTime, Weapon->GetMinSpread(), Weapon->GetMaxSpread());
		PreviousRecoil = CurrentRecoil;
	}
}

FVector UAimingComponent::GetFiringVector()
{
	if (!CameraComp)
	{
		UE_LOG(LogTemp, Error, TEXT("Aiming component's owner does not have a valid CameraComponent!"));
		return FVector();
	}

	if (GetIsADS())
	{
		return CameraComp->GetForwardVector();
	}
	else
	{
		FVector ShotStartLocation = CameraComp->GetComponentLocation();
		FVector ForwardVector = CameraComp->GetForwardVector();
		float SpreadHalfAngleRadians = FMath::DegreesToRadians(CurrentSpread);
		FVector ConeVector = FMath::VRandCone(ForwardVector, SpreadHalfAngleRadians);
		return ConeVector;
	}
}

void UAimingComponent::ApplyRecoil()
{
	APersonalWeapon* Weapon = OwningCharacter->GetPrimaryWeapon();

	if (!Weapon)
	{
		UE_LOG(LogTemp, Error, TEXT("OwningCharacter on AimingComponent has no valid PrimaryWeapon!"));
		return;
	}

	CurrentSpread = FMath::Clamp(CurrentSpread + Weapon->GetSpread(), Weapon->GetMinSpread(), Weapon->GetMaxSpread());
	CurrentVerticalRecoil -= FMath::RandRange(Weapon->GetMinVerticalRecoil(), Weapon->GetMaxVerticalRecoil());
	CurrentHorizontalRecoil += FMath::RandRange(Weapon->GetMinHorizontalRecoil(), Weapon->GetMaxHorizontalRecoil());

	CurrentSpreadDecreasePerSecond = CurrentSpread / Weapon->GetSettleSpeed();
}

bool UAimingComponent::GetIsADS()
{
	return OwningCharacter->GetIsADS();
}

bool UAimingComponent::ShouldShowCrosshairs()
{
	return !OwningCharacter->IsOperator() && !GetIsADS();
}

float UAimingComponent::GetCurrentSpread()
{
	return CurrentSpread;
}

void UAimingComponent::SetOwningCharacter(ABaseCharacter* OwningCharacter)
{
	this->OwningCharacter = OwningCharacter;
}

FRotator UAimingComponent::CalculateRecoilRotator()
{
	return FRotator(CurrentVerticalRecoil, CurrentHorizontalRecoil, 0.0f);
}
