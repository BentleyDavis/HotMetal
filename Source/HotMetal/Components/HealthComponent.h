// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "HealthComponent.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnOwnerDied);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnOwnerDamaged);

UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class HOTMETAL_API UHealthComponent : public UActorComponent
{
	GENERATED_BODY()

public:
	// Sets default values for this component's properties
	UHealthComponent();

	FOnOwnerDied OnOwnerDied;

	FOnOwnerDamaged OnOwnerDamaged;

	UFUNCTION(BlueprintCallable, Category = "Utilities")
	float GetHealthPercentage() const;

	float GetDestroyDelay();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

	AActor* Owner;

	UFUNCTION()
	void TakeAnyDamage(AActor* DamagedActor, float Damage, const class UDamageType* DamageType, class AController* InstigatedBy, AActor* DamageCauser);

	UFUNCTION()
	void OnRep_CurrentHealth();

	void OwnerDied();

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Gameplay")
	int32 MaxHealth = 100;

	UPROPERTY(ReplicatedUsing=OnRep_CurrentHealth, VisibleAnywhere, BlueprintReadWrite, Category = "Gameplay")
	int32 CurrentHealth;

	UPROPERTY(Replicated, BlueprintReadOnly, Category = "Gameplay")
	bool bIsDead = false;

	UPROPERTY(EditDefaultsOnly, Category = "Gameplay")
	float DestroyDelay = 15.0f;
};
