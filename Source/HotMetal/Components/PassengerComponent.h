// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/BoardableComponent.h"
#include "PassengerComponent.generated.h"

class ABaseCharacter;

/**
 * 
 */
UCLASS( meta=(BlueprintSpawnableComponent) )
class HOTMETAL_API UPassengerComponent : public UBoardableComponent
{
	GENERATED_BODY()
	
public:
	void TickComponent(float DeltaTime, enum ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	void OnInteract(AActor* Instigator) override;

protected:
	virtual void GetIn(ABaseCharacter* InstigatorCharacter);

	virtual void GetOut(ABaseCharacter* InstigatorCharacter);

private:
	UFUNCTION(NetMulticast, Reliable)
	void MulticastOnEnter(ABaseCharacter* LocalOperator);

	UFUNCTION(NetMulticast, Reliable)
	void MulticastOnExit(ABaseCharacter* LocalOperator);
};
