// Fill out your copyright notice in the Description page of Project Settings.

#include "OperatorComponent.h"
#include "Characters/BaseCharacter.h"
#include "Vehicles/WheeledVehicleBase.h"
#include "GameFramework/PlayerController.h"
#include "GameFramework/PawnMovementComponent.h"
#include "Components/CapsuleComponent.h"
#include "Camera/CameraComponent.h"
#include "Components/SkeletalMeshComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "HotMetal.h"
#include "Net/UnrealNetwork.h"
#include "Vehicles/BaseVehicleComponent.h"

void UOperatorComponent::BeginPlay()
{
	Super::BeginPlay();

	OwnerVehicleActor = GetOwner();

	if (!OwnerVehicleActor)
	{
		UE_LOG(LogTemp, Error, TEXT("OperatorComponent's owner invalid!"));
		return;
	}

	OwnerVehicleComponent = Cast<UBaseVehicleComponent>(OwnerVehicleActor->GetComponentByClass(UBaseVehicleComponent::StaticClass()));

	if (!OwnerVehicleComponent)
	{
		UE_LOG(LogTemp, Error, TEXT("OperatorComponent's owner does not have a valid UBaseVehicleComponent!"));
		return;
	}
}

void UOperatorComponent::GetIn(ABaseCharacter* InstigatorCharacter)
{
	Super::GetIn(InstigatorCharacter);

	InstigatorCharacter->SetCurrentVehicle(OwnerVehicleActor);

	if (OwnerVehicleComponent)
	{
		OwnerVehicleComponent->SetParentController(GetOperator()->GetController());
	}
}

void UOperatorComponent::GetOut(ABaseCharacter* InstigatorCharacter)
{
	InstigatorCharacter->SetCurrentVehicle(OwnerVehicleActor);

	Super::GetOut(InstigatorCharacter);

	if (OwnerVehicleComponent)
	{
		OwnerVehicleComponent->SetParentController(nullptr);
		OwnerVehicleComponent->OnUncontrolled();
	}
}

void UOperatorComponent::MulticastOnEnter_Implementation(ABaseCharacter* LocalOperator)
{
	Super::MulticastOnEnter_Implementation(LocalOperator);

	LocalOperator->SetCurrentVehicle(OwnerVehicleActor);

	if (LocalOperator->IsLocallyControlled())
	{
		LocalOperator->ResetPlayerInputComponent();
	}
}

void UOperatorComponent::MulticastOnExit_Implementation(ABaseCharacter* LocalOperator)
{
	Super::MulticastOnExit_Implementation(LocalOperator);

	if (GetOwnerRole() != ROLE_Authority)
	{
		LocalOperator->GetCapsuleComponent()->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);

		if (UBaseVehicleComponent * LocalVehicleComp = Cast<UBaseVehicleComponent>(LocalOperator->GetCurrentVehicle()->GetComponentByClass(UBaseVehicleComponent::StaticClass())))
		{
			LocalVehicleComp->OnUncontrolled();
		}
		LocalOperator->SetCurrentBoardableComponent(nullptr);
		LocalOperator->SetCurrentVehicle(nullptr);
	}

	if (LocalOperator->IsLocallyControlled())
	{
		LocalOperator->ResetPlayerInputComponent();
	}
}
