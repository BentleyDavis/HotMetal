// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "BasePlayerController.generated.h"

/**
 * 
 */
UCLASS()
class HOTMETAL_API ABasePlayerController : public APlayerController
{
	GENERATED_BODY()
	
public:
	void SetPawn(APawn* InPawn) override;

protected:
	UFUNCTION(BlueprintImplementableEvent)
	void OnPawnChanged(APawn* InPawn);
};
