// Fill out your copyright notice in the Description page of Project Settings.

#include "WheeledVehicleBase.h"
#include "Engine/World.h"
#include "Components/SkeletalMeshComponent.h"
#include "Net/UnrealNetwork.h"
#include "Weapons/Projectiles/ProjectileBase.h"
#include "Components/HealthComponent.h"
#include "Components/BoxComponent.h"
#include "Characters/BaseCharacter.h"
#include "WheeledVehicleMovementComponent.h"
#include "WheeledVehicleMovementComponent4W.h"
#include "BaseVehicleMovementComponent.h"
#include "GameFramework/PlayerController.h"
#include "Vehicles/BaseVehicleComponent.h"
#include "Components/OperatorComponent.h"

AWheeledVehicleBase::AWheeledVehicleBase(const class FObjectInitializer& PCIP) : Super(PCIP.SetDefaultSubobjectClass<UBaseVehicleMovementComponent>(AWheeledVehicle::VehicleMovementComponentName))
{
	VehicleComp = CreateDefaultSubobject<UBaseVehicleComponent>(TEXT("VehicleComponent"));
	OperatorComp = CreateDefaultSubobject<UOperatorComponent>(TEXT("OperatorComponent"));
	OperatorComp->SetupAttachment(GetMesh(), FName(TEXT("Door_FL")));
	HealthComp = CreateDefaultSubobject<UHealthComponent>(TEXT("HealthComponent"));
}

void AWheeledVehicleBase::BeginPlay()
{
	Super::BeginPlay();

	VehicleComp->OnSetParentController.AddDynamic(this, &AWheeledVehicleBase::SetController);
	VehicleComp->OnControlledDelegate.AddDynamic(this, &AWheeledVehicleBase::OnControlled);
	VehicleComp->OnUncontrolledDelegate.AddDynamic(this, &AWheeledVehicleBase::OnUncontrolled);
}

void AWheeledVehicleBase::OnThrottleInput(float ThrottleInput)
{
	GetVehicleMovementComponent()->SetThrottleInput(ThrottleInput);
}

void AWheeledVehicleBase::OnSteeringInput(float SteeringInput)
{
	GetVehicleMovementComponent()->SetSteeringInput(SteeringInput);
}

void AWheeledVehicleBase::StartHandbrake()
{
	GetVehicleMovementComponent()->SetHandbrakeInput(true);
}

void AWheeledVehicleBase::StopHandbrake()
{
	GetVehicleMovementComponent()->SetHandbrakeInput(false);
}

void AWheeledVehicleBase::SetController(AController* NewController)
{
	this->Controller = NewController;
}

void AWheeledVehicleBase::OnControlled()
{

}

void AWheeledVehicleBase::OnUncontrolled()
{
	UBaseVehicleMovementComponent* MovementComp = Cast<UBaseVehicleMovementComponent>(GetVehicleMovementComponent());

	if (!MovementComp)
	{
		UE_LOG(LogTemp, Error, TEXT("MovementComponent is not of type UBaseVehicleMovementComponent!"));
		return;
	}

	MovementComp->FullClearInput();
}
