// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "BaseVehicleComponent.generated.h"

class AController;

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnSetParentController, AController*, NewController);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnControlled);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnUncontrolled);

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class HOTMETAL_API UBaseVehicleComponent : public UActorComponent
{
	GENERATED_BODY()

public:
	void SetParentController(AController* NewController);

	void OnControlled();

	void OnUncontrolled();

	FOnSetParentController OnSetParentController;

	FOnControlled OnControlledDelegate;

	FOnUncontrolled OnUncontrolledDelegate;
};
