// Fill out your copyright notice in the Description page of Project Settings.


#include "BaseVehicleComponent.h"

void UBaseVehicleComponent::SetParentController(AController* NewController)
{
	OnSetParentController.Broadcast(NewController);
}

void UBaseVehicleComponent::OnControlled()
{
	OnControlledDelegate.Broadcast();
}

void UBaseVehicleComponent::OnUncontrolled()
{
	OnUncontrolledDelegate.Broadcast();
}