// Fill out your copyright notice in the Description page of Project Settings.

#include "BaseVehicleMovementComponent.h"

void UBaseVehicleMovementComponent::FullClearInput()
{
	ClearInput();
	ServerUpdateState(0, 0, 0, 0, GetCurrentGear());
}