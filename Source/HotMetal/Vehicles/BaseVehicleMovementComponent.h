// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "WheeledVehicleMovementComponent4W.h"
#include "BaseVehicleMovementComponent.generated.h"

/**
 * 
 */
UCLASS()
class HOTMETAL_API UBaseVehicleMovementComponent : public UWheeledVehicleMovementComponent4W
{
	GENERATED_BODY()
	
public:
	void FullClearInput();
};
