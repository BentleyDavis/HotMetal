// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "PhysicalMaterials/PhysicalMaterial.h"
#include "BasePhysicalMaterial.generated.h"

class USoundCue;

/**
 * 
 */
UCLASS(BlueprintType, Blueprintable, CollapseCategories, HideCategories = Object)
class HOTMETAL_API UBasePhysicalMaterial : public UPhysicalMaterial
{
	GENERATED_BODY()
	
public:
	UFUNCTION(BlueprintCallable, Category = "Utilities")
		float GetDamageMultiplier() const { return DamageMultiplier; }

	UFUNCTION(BlueprintCallable, Category = "Utilities")
		float GetPenetrationReduction() const { return PenetrationReduction; }

	UFUNCTION(BlueprintCallable, Category = "Utilities")
		UParticleSystem* GetBulletImpactParticleFX() const { return BulletImpactParticleFX; }

	UFUNCTION(BlueprintCallable, Category = "Utilities")
		USoundCue* GetBulletImpactSoundFX() const { return BulletImpactSoundFX; }

protected:
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Gameplay")
		float DamageMultiplier = 1.0;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Gameplay")
		float PenetrationReduction = 1.0f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "FX")
		UParticleSystem* BulletImpactParticleFX;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "FX")
		USoundCue* BulletImpactSoundFX;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "FX")
		USoundCue* StepSound;
};
