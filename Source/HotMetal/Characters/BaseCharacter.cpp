// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#include "BaseCharacter.h"
#include "../HotMetal.h"
#include "Interfaces/Interactable.h"
#include "Animation/AnimInstance.h"
#include "Components/CapsuleComponent.h"
#include "Components/InputComponent.h"
#include "Camera/CameraComponent.h"
#include "Kismet/GameplayStatics.h"
#include "GameFramework/PlayerController.h"
#include "GameFramework/PlayerState.h"
#include "Net/UnrealNetwork.h"
#include "../Vehicles/WheeledVehicleBase.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Components/AimingComponent.h"
#include "Components/HealthComponent.h"
#include "Components/PassengerComponent.h"
#include "Components/OperatorComponent.h"
#include "DrawDebugHelpers.h"
#include "TimerManager.h"
#include "GameFramework/SpectatorPawn.h"
#include "Components/BaseCharacterMovementComponent.h"
#include "Components/TimelineComponent.h"
#include "ConstructorHelpers.h"

DEFINE_LOG_CATEGORY_STATIC(LogFPChar, Warning, All);

//////////////////////////////////////////////////////////////////////////
// ABaseCharacter

ABaseCharacter::ABaseCharacter(const class FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer.SetDefaultSubobjectClass<UBaseCharacterMovementComponent>(ACharacter::CharacterMovementComponentName))
{
	SetRootComponent(GetCapsuleComponent());
	// Set size for collision capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// Set our turn rates for input
	BaseTurnRate = 45.f;
	BaseLookUpRate = 45.f;

	// Create a CameraComponent	
	FirstPersonCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("FirstPersonCamera"));
	FirstPersonCameraComponent->SetupAttachment(GetCapsuleComponent());
	FirstPersonCameraComponent->RelativeLocation = FVector(0, 0, 64.f); // Position the camera
	FirstPersonCameraComponent->bUsePawnControlRotation = true;

	// Create a mesh component that will be used when being viewed from a '1st person' view (when controlling this pawn)
	Mesh1P = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("CharacterMesh1P"));
	Mesh1P->SetOnlyOwnerSee(true);				// Set so only owner can see mesh
	Mesh1P->SetupAttachment(FirstPersonCameraComponent);	// Attach mesh to FirstPersonCameraComponent
	Mesh1P->bCastDynamicShadow = false;			// Disallow mesh to cast dynamic shadows
	Mesh1P->CastShadow = false;				// Disallow mesh to cast other shadows

	// Create a mesh component that will be used when being viewed from a '3rd person' view (when not controlling this pawn)
	Mesh3P = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("CharacterMesh3P"));
	Mesh3P->SetOwnerNoSee(true);				// Set so only owner can see mesh
	Mesh3P->SetupAttachment(FirstPersonCameraComponent);	// Attach mesh to FirstPersonCameraComponent
	Mesh3P->bCastDynamicShadow = false;			// Disallow mesh to cast dynamic shadows
	Mesh3P->CastShadow = false;				// Disallow mesh to cast other shadows

	// Create timeline component for smooth crouching
	CrouchTimeline = CreateDefaultSubobject<UTimelineComponent>("CrouchTimelineComponent");

	// Create an aiming component
	AimingComp = CreateDefaultSubobject<UAimingComponent>(TEXT("AimingComponent"));
	AimingComp->SetOwningCharacter(this);
	HealthComp = CreateDefaultSubobject<UHealthComponent>(TEXT("HealthComponent"));

	// Set weapon damage and range
	WeaponRange = 5000.0f;
	WeaponDamage = 500000.0f;
	InteractionRange = 150.0f;
}

void ABaseCharacter::BeginPlay()
{
	Super::BeginPlay();

	if (!CrouchCurve)
	{
		UE_LOG(LogTemp, Error, TEXT("CrouchCurve on BaseCharacter is null!"));
		return;
	}

	// Initialize smooth crouching variables
	FOnTimelineFloat CrouchTimelineCallback;
	CrouchTimelineCallback.BindUFunction(this, FName("CrouchTimelineUpdate"));
	CrouchTimeline->AddInterpFloat(CrouchCurve, CrouchTimelineCallback);
	StandingStartingHalfHeight = GetCapsuleComponent()->GetScaledCapsuleHalfHeight();
	Mesh3PRelativeStartingLocation = GetMesh3P()->RelativeLocation;

	HealthComp->OnOwnerDied.AddUniqueDynamic(this, &ABaseCharacter::PlayerDied);
	HealthComp->OnOwnerDamaged.AddUniqueDynamic(this, &ABaseCharacter::PlayerDamaged);

	if (Role == ROLE_Authority)
	{
		ServerSpawnDefaultWeapons();
	}
}

void ABaseCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (IsLocallyControlled())
	{
		CheckForInteractableComponents();
		FVector_NetQuantize100 LookRotation = GetFirstPersonCameraComponent()->GetComponentRotation().Vector();
		ServerUpdateLookRotation(LookRotation);
	}

	JustJumped = false;
}

//////////////////////////////////////////////////////////////////////////
// Input

void ABaseCharacter::SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent)
{
	check(PlayerInputComponent);

	// Clear all inputs
	while (PlayerInputComponent->GetNumActionBindings() > 0)
	{
		PlayerInputComponent->RemoveActionBinding(0);
	}
	while (PlayerInputComponent->AxisBindings.Num() > 0)
	{
		PlayerInputComponent->AxisBindings.RemoveAt(0);
	}

	PlayerInputComponent->BindAction("Interact", IE_Pressed, this, &ABaseCharacter::Interact);
	PlayerInputComponent->BindAxis("Turn", this, &ABaseCharacter::Turn);
	PlayerInputComponent->BindAxis("TurnRate", this, &ABaseCharacter::TurnAtRate);
	PlayerInputComponent->BindAxis("LookUp", this, &ABaseCharacter::LookUp);
	PlayerInputComponent->BindAxis("LookUpRate", this, &ABaseCharacter::LookUpAtRate);

	if (!CurrentVehicle)
	{
		// BaseCharacter Input
		PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &ABaseCharacter::Jump);
		PlayerInputComponent->BindAction("Jump", IE_Released, this, &ACharacter::StopJumping);
		PlayerInputComponent->BindAction("Crouch", IE_Pressed, this, &ABaseCharacter::StartCrouch);
		PlayerInputComponent->BindAction("Crouch", IE_Released, this, &ABaseCharacter::StopCrouch);
		PlayerInputComponent->BindAction("Fire", IE_Pressed, this, &ABaseCharacter::OnPressTrigger);
		PlayerInputComponent->BindAction("Fire", IE_Released, this, &ABaseCharacter::OnReleaseTrigger);
		PlayerInputComponent->BindAction("Reload", IE_Released, this, &ABaseCharacter::OnReload);
		PlayerInputComponent->BindAction("ADS", IE_Pressed, this, &ABaseCharacter::OnADS);
		PlayerInputComponent->BindAction("ActivatePrimary", IE_Pressed, this, &ABaseCharacter::ActivatePrimaryWeapon);
		PlayerInputComponent->BindAction("ActivateSecondary", IE_Pressed, this, &ABaseCharacter::ActivateSecondaryWeapon);
		PlayerInputComponent->BindAction("ActivateFists", IE_Pressed, this, &ABaseCharacter::ActivateFists);
		PlayerInputComponent->BindAxis("MoveForward", this, &ABaseCharacter::MoveForward);
		PlayerInputComponent->BindAxis("MoveRight", this, &ABaseCharacter::MoveRight);
	}
	else
	{
		// WheeledVehicleBase Input
		PlayerInputComponent->BindAction("Handbrake", IE_Pressed, this, &ABaseCharacter::WheeledVehicleStartHandbrake);
		PlayerInputComponent->BindAction("Handbrake", IE_Released, this, &ABaseCharacter::WheeledVehicleStopHandbrake);
		PlayerInputComponent->BindAxis("WheeledVehicleForward", this, &ABaseCharacter::WheeledVehicleForward);
		PlayerInputComponent->BindAxis("WheeledVehicleRight", this, &ABaseCharacter::WheeledVehicleRight);
	}
}

void ABaseCharacter::ResetPlayerInputComponent()
{
	SetupPlayerInputComponent(InputComponent);
}

void ABaseCharacter::PlayerDamaged()
{
	OnPlayerDamaged();
}

void ABaseCharacter::PlayerDied()
{
	GetCapsuleComponent()->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	GetMesh3P()->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	ServerPlayerDied();
}

void ABaseCharacter::ServerPlayerDied_Implementation()
{
	if (CurrentBoardableComponent)
	{
		ServerInteract(CurrentBoardableComponent);
	}
	GetCapsuleComponent()->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	GetMesh3P()->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	MulticastPlayerDied();
	APlayerController* PC = Cast<APlayerController>(GetController());
	DetachFromControllerPendingDestroy();
	PC->PlayerState->bIsSpectator = true;
	PC->ChangeState(NAME_Spectating);
	PC->ClientGotoState(NAME_Spectating);
	FTimerHandle DestroyTimer;
	GetWorldTimerManager().SetTimer(DestroyTimer, this, &ABaseCharacter::DestroyCharacter, HealthComp->GetDestroyDelay());
}

bool ABaseCharacter::ServerPlayerDied_Validate()
{
	return true;
}

void ABaseCharacter::MulticastPlayerDied_Implementation()
{
	if (IsLocallyControlled())
	{
		OnPlayerDied();
	}

	GetMesh3P()->SetVisibility(false, true);
	GetCapsuleComponent()->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	GetMesh3P()->SetCollisionEnabled(ECollisionEnabled::NoCollision);
}

void ABaseCharacter::DestroyCharacter()
{
	PrimaryWeapon->Destroy();
	Destroy();
}

void ABaseCharacter::ServerSpawnDefaultWeapons_Implementation()
{
	FActorSpawnParameters SpawnParameters = FActorSpawnParameters();
	SpawnParameters.Instigator = this;
	SpawnParameters.Owner = this;
	SpawnParameters.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;

	if (DefaultPrimaryWeaponClass)
	{
		PrimaryWeapon = GetWorld()->SpawnActor<APersonalWeapon>(DefaultPrimaryWeaponClass, SpawnParameters);
		PrimaryWeapon->SetupWielder(this);
		HideWeapon(PrimaryWeapon);
		PrimaryWeapon->GetFPSkeletalMeshComp()->SetVisibility(false);
		PrimaryWeapon->GetTPSkeletalMeshComp()->SetVisibility(false);
	}

	if (DefaultSecondaryWeaponClass)
	{
		SecondaryWeapon = GetWorld()->SpawnActor<APersonalWeapon>(DefaultSecondaryWeaponClass, SpawnParameters);
		SecondaryWeapon->SetupWielder(this);
		HideWeapon(SecondaryWeapon);
		SecondaryWeapon->GetFPSkeletalMeshComp()->SetVisibility(false);
		SecondaryWeapon->GetTPSkeletalMeshComp()->SetVisibility(false);
	}

	if (FistClass)
	{
		Fists = GetWorld()->SpawnActor<APersonalWeapon>(FistClass, SpawnParameters);
		Fists->SetupWielder(this);
		HideWeapon(Fists);
		Fists->GetFPSkeletalMeshComp()->SetVisibility(false);
		Fists->GetTPSkeletalMeshComp()->SetVisibility(false);
	}
	else
	{
		UE_LOG(LogTemp, Error, TEXT("FistClass is invalid!"));
		return;
	}

	if (PrimaryWeapon)
	{
		// TODO These should be server function calls
		/*Server*/UnholsterWeapon(PrimaryWeapon);
	}
	else if (SecondaryWeapon)
	{
		/*Server*/UnholsterWeapon(SecondaryWeapon);
	}
	else
	{
		/*Server*/UnholsterWeapon(Fists);
	}
}

bool ABaseCharacter::ServerSpawnDefaultWeapons_Validate()
{
	return true;
}

void ABaseCharacter::ActivateWeapon(APersonalWeapon* NewWeapon)
{
	if (NewWeapon && NewWeapon != ActiveWeapon && ActionState == EActionState::IDLE)
	{
		ActionState = EActionState::SWITCHING_WEAPONS;
		if (IsLocallyControlled())
		{
			ServerNotifySwapWeapon(NewWeapon);
		}
		UE_LOG(LogTemp, Warning, TEXT("TODO: Play lower-weapon animation on %s"), *(ActiveWeapon->GetName()));
		float AnimationDuration = 0.0f;
		if (ActiveWeapon && ActiveWeapon->GetFPUnequipAnimation() && ActiveWeapon->GetTPUnequipAnimation())
		{
			UAnimMontage* FPEquipMontage = ActiveWeapon->GetFPUnequipAnimation();
			UAnimMontage* TPEquipMontage = ActiveWeapon->GetTPUnequipAnimation();
			// Calculate how quickly we should play the animation so that it aligns with the EquipTime of the weapon
			float FPAnimationRate = FPEquipMontage->GetPlayLength() / ActiveWeapon->GetUnequipTime();
			float TPAnimationRate = TPEquipMontage->GetPlayLength() / ActiveWeapon->GetUnequipTime();
			AnimationDuration = FMath::Abs(GetMesh1P()->GetAnimInstance()->Montage_Play(FPEquipMontage, FPAnimationRate, EMontagePlayReturnType::Duration, FPEquipMontage->GetPlayLength()));
			GetMesh3P()->GetAnimInstance()->Montage_Play(TPEquipMontage, TPAnimationRate, EMontagePlayReturnType::Duration);
		}
		else
		{
			UE_LOG(LogTemp, Error, TEXT("Active weapon is either invalid or has no valid UnequipAnimation montage"));
		}

		// Start timer for hiding current weapon
		FTimerHandle HideWeaponHandle;
		FTimerDelegate HideWeaponDelegate;
		HideWeaponDelegate.BindUFunction(this, FName("HideWeapon"), ActiveWeapon);
		/* The timer is set for (AnimationDuration - .1f) so that the equip and unequip animations are blended over each other.
		 * This prevents the animations from blending into the idle pose between the two animations, and makes it so that the weapon
		 * swap takes place off-screen. */
		GetWorldTimerManager().SetTimer(HideWeaponHandle, HideWeaponDelegate, AnimationDuration - .1f, false);
		// Start timer for unholstering new weapon
		FTimerHandle UnholsterWeaponHandle;
		FTimerDelegate UnholsterWeaponDelegate;
		UnholsterWeaponDelegate.BindUFunction(this, FName("UnholsterWeapon"), NewWeapon);
		GetWorldTimerManager().SetTimer(UnholsterWeaponHandle, UnholsterWeaponDelegate, AnimationDuration - .1f, false);
	}
}

void ABaseCharacter::ActivatePrimaryWeapon()
{
	ActivateWeapon(PrimaryWeapon);
}

void ABaseCharacter::ActivateSecondaryWeapon()
{
	ActivateWeapon(SecondaryWeapon);
}

void ABaseCharacter::ActivateFists()
{
	ActivateWeapon(Fists);
}

void ABaseCharacter::ServerNotifySwapWeapon_Implementation(APersonalWeapon* NewWeapon)
{
	MulticastNotifySwapWeapon(NewWeapon);
	ActivateWeapon(NewWeapon);
}

bool ABaseCharacter::ServerNotifySwapWeapon_Validate(APersonalWeapon* NewWeapon)
{
	return true;
}

void ABaseCharacter::MulticastNotifySwapWeapon_Implementation(APersonalWeapon* NewWeapon)
{
	if (!IsLocallyControlled())
	{
		ActivateWeapon(NewWeapon);
	}
}

void ABaseCharacter::HideWeapon(APersonalWeapon* Weapon)
{
	if (Role == ROLE_Authority)
	{
		if (Weapon)
		{
			UE_LOG(LogTemp, Warning, TEXT("Hiding weapon"));
			Weapon->RepVisible = false;
		}
		else
		{
			UE_LOG(LogTemp, Error, TEXT("Cannot hide weapon, reference is invalid"));
		}
	}
}

void ABaseCharacter::UnhideWeapon(APersonalWeapon* Weapon)
{
	if (Role == ROLE_Authority)
	{
		if (Weapon)
		{
			UE_LOG(LogTemp, Warning, TEXT("Showing weapon"));
			Weapon->RepVisible = true;
		}
		else
		{
			UE_LOG(LogTemp, Error, TEXT("Cannot unhide weapon, reference is invalid"));
		}
	}
}

void ABaseCharacter::UnholsterWeapon(APersonalWeapon* Weapon)
{
	ActiveWeapon = Weapon;
	OnActiveWeaponChanged();
	UnhideWeapon(Weapon);

	UE_LOG(LogTemp, Warning, TEXT("TODO: Play raise-weapon animation on %s"), *(ActiveWeapon->GetName()));
	float AnimationDuration = 0.0f;
	if (ActiveWeapon && ActiveWeapon->GetFPEquipAnimation() && ActiveWeapon->GetTPEquipAnimation())
	{
		UAnimMontage* FPEquipMontage = ActiveWeapon->GetFPEquipAnimation();
		UAnimMontage* TPEquipMontage = ActiveWeapon->GetTPEquipAnimation();
		float FPAnimationRate = FPEquipMontage->GetPlayLength() / ActiveWeapon->GetEquipTime();
		float TPAnimationRate = TPEquipMontage->GetPlayLength() / ActiveWeapon->GetEquipTime();
		// This animation does not stop all previous montages because it needs to properly blend with the unequip montage that is still finishing up
		AnimationDuration = GetMesh1P()->GetAnimInstance()->Montage_Play(FPEquipMontage, FPAnimationRate, EMontagePlayReturnType::Duration, 0.0f, false);
		GetMesh3P()->GetAnimInstance()->Montage_Play(TPEquipMontage, TPAnimationRate, EMontagePlayReturnType::Duration, 0.0f, false);
	}
	else
	{
		UE_LOG(LogTemp, Error, TEXT("Active weapon is either invalid or has no valid EquipAnimation montage"));
	}

	FTimerHandle OnWeaponReadyHandle;
	GetWorldTimerManager().SetTimer(OnWeaponReadyHandle, this, &ABaseCharacter::OnWeaponReady, AnimationDuration);
}

void ABaseCharacter::OnWeaponReady()
{
	ActionState = EActionState::IDLE;
	UE_LOG(LogTemp, Warning, TEXT("%s ready"), *(ActiveWeapon->GetName()));
}

void ABaseCharacter::CheckForInteractableComponents()
{
	FHitResult result = GetFirstInteractableInReach();

	// Make sure the result is interactable
	if (IInteractable* InteractableComponent = Cast<IInteractable>(result.GetComponent()))
	{
		// If we aren't already looking at an interactable object
		if (!InteractableObject)
		{
			InteractableObject = result.GetComponent();
			InteractableComponent->StartFocus();
		}
		else // We were looking at an interactable object in the previous frame; make sure to check if it's a different object
		{
			IInteractable* ComparableInteractableObject = Cast<IInteractable>(InteractableObject);
			
			// At some point InteractableObject was assigned an object that did not implement IInteractable: Error
			if (!ComparableInteractableObject)
			{
				UE_LOG(LogTemp, Error, TEXT("InteractableObject on %s does not implement IInteractable!"), *(GetName()));
				return;
			}

			// The object in this frame is different from the last frame, update accordingly
			if (InteractableComponent != ComparableInteractableObject)
			{
				IInteractable* PreviousInteractable = Cast<IInteractable>(InteractableObject);
				PreviousInteractable->StopFocus();
				InteractableObject = result.GetComponent();
				InteractableComponent->StartFocus();
			}
		}
	}
	else
	{
		//UE_LOG(LogTemp, Warning, TEXT("Does not implement IInteractable!"));
		if (InteractableObject)
		{
			IInteractable* InteractableComponent = Cast<IInteractable>(InteractableObject);
			InteractableComponent->StopFocus();
			InteractableObject = nullptr;
		}
	}
}

FHitResult ABaseCharacter::GetFirstInteractableInReach()
{
	FVector RaycastStartPoint;
	FVector RaycastEndPoint;
	GetRaycastEndPoints(RaycastStartPoint, RaycastEndPoint);

	FHitResult HitResult;
	FCollisionQueryParams TraceParameters(FName(TEXT("")), false, this);
	GetWorld()->LineTraceSingleByChannel(
		OUT HitResult,
		RaycastStartPoint,
		RaycastEndPoint,
		COLLISION_INTERACT,
		TraceParameters
	);

	return HitResult;
}


void ABaseCharacter::GetRaycastEndPoints(FVector &OutRaycastStart, FVector &OutRaycastEnd)
{
	OutRaycastStart = GetActorLocation();
	FRotator PlayerRotation = GetActorRotation();

	GetWorld()->GetFirstPlayerController()->GetPlayerViewPoint(OutRaycastStart, PlayerRotation);

	OutRaycastEnd = (OutRaycastStart + (PlayerRotation.Vector() * InteractionRange));

	return;
}

void ABaseCharacter::Turn(float Val)
{
	if (!CurrentBoardableComponent)
	{
		AddControllerYawInput(Val);
	}
	else
	{
		APlayerController* PC = Cast<APlayerController>(GetController());

		if (PC)
		{
			GetFirstPersonCameraComponent()->AddRelativeRotation(FRotator(0.0f, Val * PC->InputYawScale, 0.0f));
		}
	}
}

void ABaseCharacter::LookUp(float Val)
{
	if (!CurrentBoardableComponent)
	{
		AddControllerPitchInput(Val);
	}
	else
	{
		APlayerController* PC = Cast<APlayerController>(GetController());

		if (PC)
		{
			FRotator CurrentRotation = GetFirstPersonCameraComponent()->RelativeRotation;
			float NewPitch = CurrentRotation.Pitch + Val * PC->InputPitchScale;
			NewPitch = FMath::ClampAngle(NewPitch, -80.0f, 80.0f);
			CurrentRotation.Pitch = NewPitch;
			GetFirstPersonCameraComponent()->SetRelativeRotation(CurrentRotation);
		}
	}
}

void ABaseCharacter::ClearLookInput()
{
	GetController()->SetControlRotation(FRotator(0.0f, 0.0f, 0.0f));
}

void ABaseCharacter::Jump()
{
	JustJumped = true;
	ServerJump();
	Super::Jump();
}

void ABaseCharacter::ServerJump_Implementation()
{
	JustJumped = true;
	MulticastJump();
}

bool ABaseCharacter::ServerJump_Validate()
{
	return true;
}

void ABaseCharacter::MulticastJump_Implementation()
{
	JustJumped = true;
}

void ABaseCharacter::StartCrouch()
{
	Crouch(false);
}

void ABaseCharacter::StopCrouch()
{
	UnCrouch(false);
}

void ABaseCharacter::OnPressTrigger()
{
	if (!ActiveWeapon)
	{
		UE_LOG(LogTemp, Error, TEXT("No active weapon assigned!"));
		return;
	}

	if (ActionState == EActionState::IDLE)
	{
		ActiveWeapon->PressTrigger();
	}
}

void ABaseCharacter::OnReleaseTrigger()
{
	if (!ActiveWeapon)
	{
		UE_LOG(LogTemp, Error, TEXT("No active weapon assigned!"));
		return;
	}

	ActiveWeapon->ReleaseTrigger();
}

void ABaseCharacter::OnReload()
{
	if (!ActiveWeapon)
	{
		UE_LOG(LogTemp, Error, TEXT("No active weapon assigned!"));
		return;
	}

	if (ActionState == EActionState::IDLE)
	{
		ActiveWeapon->StartReload();
	}
}

void ABaseCharacter::OnADS()
{
	if (ActiveWeapon)
	{
		if (!ActiveWeapon->GetIsReloading())
		{
			IsADS = !IsADS;
			ServerUpdateIsADS(IsADS);
		}
	}
}

void ABaseCharacter::BeginTouch(const ETouchIndex::Type FingerIndex, const FVector Location)
{
	// If touch is already pressed check the index. If it is not the same as the current touch assume a second touch and thus we want to fire
	if (TouchItem.bIsPressed == true)
	{
		if (TouchItem.FingerIndex != FingerIndex)
		{
			// OnFire();
		}
	}
	else
	{
		// Cache the finger index and touch location and flag we are processing a touch
		TouchItem.bIsPressed = true;
		TouchItem.FingerIndex = FingerIndex;
		TouchItem.Location = Location;
		TouchItem.bMoved = false;
	}
}

void ABaseCharacter::EndTouch(const ETouchIndex::Type FingerIndex, const FVector Location)
{
	// If we didn't record the start event do nothing, or this is a different index
	if ((TouchItem.bIsPressed == false) || (TouchItem.FingerIndex != FingerIndex))
	{
		return;
	}

	// If the index matches the start index and we didn't process any movement we assume we want to fire
	if ((FingerIndex == TouchItem.FingerIndex) && (TouchItem.bMoved == false))
	{
		// OnFire();
	}

	// Flag we are no longer processing the touch event
	TouchItem.bIsPressed = false;
}

void ABaseCharacter::TouchUpdate(const ETouchIndex::Type FingerIndex, const FVector Location)
{
	// If we are processing a touch event and this index matches the initial touch event process movement
	if ((TouchItem.bIsPressed == true) && (TouchItem.FingerIndex == FingerIndex))
	{
		if (GetWorld() != nullptr)
		{
			UGameViewportClient* ViewportClient = GetWorld()->GetGameViewport();
			if (ViewportClient != nullptr)
			{
				FVector MoveDelta = Location - TouchItem.Location;
				FVector2D ScreenSize;
				ViewportClient->GetViewportSize(ScreenSize);
				FVector2D ScaledDelta = FVector2D(MoveDelta.X, MoveDelta.Y) / ScreenSize;
				if (FMath::Abs(ScaledDelta.X) >= (4.0f / ScreenSize.X))
				{
					TouchItem.bMoved = true;
					float Value = ScaledDelta.X * BaseTurnRate;
					AddControllerYawInput(Value);
				}
				if (FMath::Abs(ScaledDelta.Y) >= (4.0f / ScreenSize.Y))
				{
					TouchItem.bMoved = true;
					float Value = ScaledDelta.Y* BaseTurnRate;
					AddControllerPitchInput(Value);
				}
				TouchItem.Location = Location;
			}
			TouchItem.Location = Location;
		}
	}
}

void ABaseCharacter::MoveForward(float Value)
{
	if (Value != 0.0f)
	{
		// Add movement in that direction
		AddMovementInput(GetActorForwardVector(), Value);
	}
}

void ABaseCharacter::MoveRight(float Value)
{
	if (Value != 0.0f)
	{
		// Add movement in that direction
		AddMovementInput(GetActorRightVector(), Value);
	}
}

void ABaseCharacter::TurnAtRate(float Rate)
{
	// Calculate delta for this frame from the rate information
	AddControllerYawInput(Rate * BaseTurnRate * GetWorld()->GetDeltaSeconds());
}

void ABaseCharacter::LookUpAtRate(float Rate)
{
	// Calculate delta for this frame from the rate information
	AddControllerPitchInput(Rate * BaseLookUpRate * GetWorld()->GetDeltaSeconds());
}

void ABaseCharacter::WheeledVehicleForward(float Val)
{
	if (AWheeledVehicleBase* WheeledVehicle = Cast<AWheeledVehicleBase>(CurrentVehicle))
	{
		WheeledVehicle->OnThrottleInput(Val);
	}
}

void ABaseCharacter::WheeledVehicleRight(float Val)
{
	if (AWheeledVehicleBase* WheeledVehicle = Cast<AWheeledVehicleBase>(CurrentVehicle))
	{
		WheeledVehicle->OnSteeringInput(Val);
	}
}

void ABaseCharacter::WheeledVehicleStartHandbrake()
{
	if (AWheeledVehicleBase* WheeledVehicle = Cast<AWheeledVehicleBase>(CurrentVehicle))
	{
		WheeledVehicle->StartHandbrake();
	}
}

void ABaseCharacter::WheeledVehicleStopHandbrake()
{
	if (AWheeledVehicleBase* WheeledVehicle = Cast<AWheeledVehicleBase>(CurrentVehicle))
	{
		WheeledVehicle->StopHandbrake();
	}
}

FHitResult ABaseCharacter::WeaponTrace(const FVector& StartTrace, const FVector& EndTrace) const
{
	// Perform trace to retrieve hit info
	FCollisionQueryParams TraceParams(SCENE_QUERY_STAT(WeaponTrace), true, Instigator);
	//TraceParams.bTraceAsyncScene = true;
	TraceParams.bReturnPhysicalMaterial = true;

	FHitResult Hit(ForceInit);
	GetWorld()->LineTraceSingleByChannel(
		Hit,
		StartTrace,
		EndTrace,
		COLLISION_WEAPON,
		TraceParams
	);

	return Hit;
}

void ABaseCharacter::TryEnableTouchscreenMovement(UInputComponent* PlayerInputComponent)
{
	PlayerInputComponent->BindTouch(EInputEvent::IE_Pressed, this, &ABaseCharacter::BeginTouch);
	PlayerInputComponent->BindTouch(EInputEvent::IE_Released, this, &ABaseCharacter::EndTouch);
	PlayerInputComponent->BindTouch(EInputEvent::IE_Repeat, this, &ABaseCharacter::TouchUpdate);
}

void ABaseCharacter::Interact()
{
	if (CurrentBoardableComponent)
	{
		IInteractable* CurrentInteractable = Cast<IInteractable>(CurrentBoardableComponent);

		if (!CurrentInteractable)
		{
			UE_LOG(LogTemp, Error, TEXT("CurrentBoardableComponent on %s does not implement IInteractable!"), *(GetName()));
			return;
		}

		ServerInteract(CurrentBoardableComponent);
	}
	else
	{
		if (InteractableObject)
		{
			if (IInteractable* Interactable = Cast<IInteractable>(InteractableObject))
			{
				//if (Cast<UOperatorComponent>(InteractableObject))
				//{
				//	
				//}

				ServerInteract(InteractableObject);
			}
			else
			{
				UE_LOG(LogTemp, Error, TEXT("InteractableObject on %s does no implement IInteractable!"), *(GetName()));
			}
		}
	}
}

void ABaseCharacter::ServerInteract_Implementation(UObject* Interactable)
{
	if (IInteractable* InteractableObj = Cast<IInteractable>(Interactable))
	{
		InteractableObj->OnInteract(this);
	}
	else
	{
		UE_LOG(LogTemp, Error, TEXT("InteractableObject on %s does no implement IInteractable!"), *(GetName()));
	}
}

bool ABaseCharacter::ServerInteract_Validate(UObject* Interactable)
{
	return true;
}

void ABaseCharacter::SetCurrentVehicle(AActor* Vehicle)
{
	this->CurrentVehicle = Vehicle;
}

AActor* ABaseCharacter::GetCurrentVehicle()
{
	return CurrentVehicle;
}

void ABaseCharacter::SetCurrentBoardableComponent(UBoardableComponent* Boardable)
{
	this->CurrentBoardableComponent = Boardable;
}

UBoardableComponent* ABaseCharacter::GetCurrentBoardableComponent()
{
	return CurrentBoardableComponent;
}

APersonalWeapon* ABaseCharacter::GetPrimaryWeapon()
{
	return PrimaryWeapon;
}

bool ABaseCharacter::GetIsADS()
{
	return IsADS;
}

void ABaseCharacter::SetIsADS(bool IsADS)
{
	this->IsADS = IsADS;
	ServerUpdateIsADS(IsADS);
}

FVector_NetQuantize100 ABaseCharacter::GetLookRotation()
{
	return LookRotation;
}

void ABaseCharacter::OnStartCrouch(float HeightAdjust, float ScaledHeightAdjust)
{
	Super::OnStartCrouch(HeightAdjust, ScaledHeightAdjust);

	if (!CrouchTimeline)
	{
		UE_LOG(LogTemp, Error, TEXT("CrouchTimeline is not valid on ABaseCharacter!"));
		return;
	}
	
	CrouchTimeline->Play();
}

void ABaseCharacter::OnEndCrouch(float HalfHeightAdjust, float ScaledHalfHeightAdjust)
{
	Super::OnEndCrouch(HalfHeightAdjust, ScaledHalfHeightAdjust);

	if (!CrouchTimeline)
	{
		UE_LOG(LogTemp, Error, TEXT("CrouchTimeline is not valid on ABaseCharacter!"));
		return;
	}

	CrouchTimeline->Reverse();
}

void ABaseCharacter::CrouchTimelineUpdate(float Value)
{
	PreviousHalfHeight = GetCapsuleComponent()->GetScaledCapsuleHalfHeight();
	GetCapsuleComponent()->SetCapsuleHalfHeight(Value * StandingStartingHalfHeight);
	GetMesh3P()->SetRelativeLocation(
		FVector(
			Mesh3PRelativeStartingLocation.X,
			Mesh3PRelativeStartingLocation.Y,
			Mesh3PRelativeStartingLocation.Z + (StandingStartingHalfHeight - (StandingStartingHalfHeight * Value))
		)
	);

	if (GetCharacterMovement()->IsMovingOnGround())
	{
		FVector CurrentLocation = GetActorLocation();
		CurrentLocation.Z = CurrentLocation.Z + ((Value * StandingStartingHalfHeight) - PreviousHalfHeight);
		SetActorLocation(CurrentLocation);
	}
}

void ABaseCharacter::SetIsCrouched(bool IsCrouched)
{
	bIsCrouched = IsCrouched;
}

bool ABaseCharacter::IsPassenger()
{
	return Cast<UPassengerComponent>(CurrentBoardableComponent) != nullptr;
}

bool ABaseCharacter::IsOperator()
{
	return Cast<UOperatorComponent>(CurrentBoardableComponent) != nullptr;
}

bool ABaseCharacter::IsInVehicle()
{
	return IsPassenger() || IsOperator();
}

#pragma region Replication

void ABaseCharacter::ServerUpdateLookRotation_Implementation(FVector_NetQuantize100 LookRotation)
{
	this->LookRotation = LookRotation;
}

bool ABaseCharacter::ServerUpdateLookRotation_Validate(FVector_NetQuantize100 LookRotation)
{
	return true;
}

void ABaseCharacter::ServerUpdateIsADS_Implementation(bool NewIsADS)
{
	this->IsADS = NewIsADS;
}

bool ABaseCharacter::ServerUpdateIsADS_Validate(bool NewIsADS)
{
	return true;
}

void ABaseCharacter::OnRep_ActiveWeapon()
{
	OnActiveWeaponChanged();
}

void ABaseCharacter::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(ABaseCharacter, InteractableObject);
	DOREPLIFETIME(ABaseCharacter, ActiveWeapon);
	DOREPLIFETIME(ABaseCharacter, PrimaryWeapon);
	DOREPLIFETIME(ABaseCharacter, SecondaryWeapon);
	DOREPLIFETIME(ABaseCharacter, IsADS);
	DOREPLIFETIME(ABaseCharacter, LookRotation);
}

#pragma endregion