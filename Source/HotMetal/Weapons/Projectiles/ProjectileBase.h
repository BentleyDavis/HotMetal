// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "ProjectileBase.generated.h"

UCLASS()
class HOTMETAL_API AProjectileBase : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AProjectileBase();

	/****************************************************
	* FUNCTIONS											*
	*****************************************************/
	// Called every frame
	virtual void Tick(float DeltaTime) override;

protected:
	/****************************************************
	* FUNCTIONS											*
	*****************************************************/
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

private:
	/****************************************************
	* FUNCTIONS											*
	*****************************************************/
	UFUNCTION()
	void OnHit(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit);

	UFUNCTION()
	void OnRep_Impact();

	/****************************************************
	* PROPERTIES										*
	*****************************************************/
	UPROPERTY(VisibleDefaultsOnly)
	UStaticMeshComponent* StaticMeshComp;

	UPROPERTY(VisibleDefaultsOnly)
	UParticleSystemComponent* ParticleSystemComp;

	UPROPERTY(EditDefaultsOnly, Category = "Setup")
	TSubclassOf<UDamageType> DamageType;

	UPROPERTY(ReplicatedUsing = OnRep_Impact)
	FVector ImpactLocation;
};
