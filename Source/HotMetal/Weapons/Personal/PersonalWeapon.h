// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Animation/SkeletalMeshActor.h"
#include "PersonalWeapon.generated.h"

class USkeletalMeshComponent;
class ABaseCharacter;
class UAimingComponent;
class UAnimMontage;
class USoundCue;

// Contains information about every shot fired to replicated effects to clients
USTRUCT()
struct FRaycastShot
{
	GENERATED_BODY()

public:
	UPROPERTY()
		float LastShotTime;

	UPROPERTY()
		FVector_NetQuantize TraceFrom;

	UPROPERTY()
		FVector_NetQuantize TraceTo;
};

UENUM(BlueprintType)
enum class EFireStyle : uint8
{
	SEMI_AUTO		UMETA(DisplayName = "Semi Auto"),
	SEMI_AUTO_RAPID	UMETA(DisplayName = "Semi Auto with Rapid Fire"),
	FULL_AUTO		UMETA(DisplayName = "Full Auto"),
	FULL_AUTO_RAPID	UMETA(DisplayName = "Full Auto with Rapid Fire")/*,
	BURST			UMETA(DisplayName = "Burst")*/
};

UENUM(BlueprintType)
enum class EWeaponAnimationType : uint8
{
	FISTS		UMETA(DisplayName = "Fists"),
	RIFLE		UMETA(DisplayName = "Rifle"),
	PISTOL		UMETA(DisplayName = "Pistol"),
	SMG			UMETA(DisplayName = "SMG")
};

UCLASS()
class HOTMETAL_API APersonalWeapon : public AActor
{
	GENERATED_BODY()

public:
	APersonalWeapon();

	void PressTrigger();

	void ReleaseTrigger();

	void StartReload();

	void SetupWielder(ABaseCharacter* Wielder);

	UFUNCTION(BlueprintCallable)
		USkeletalMeshComponent* GetFPSkeletalMeshComp();

	UFUNCTION(BlueprintCallable)
		USkeletalMeshComponent* GetTPSkeletalMeshComp();

	UFUNCTION(BlueprintCallable)
		TSubclassOf<UUserWidget> GetWeaponHUDWidget();

	float GetMinVerticalRecoil();

	float GetMaxVerticalRecoil();

	float GetMinHorizontalRecoil();

	float GetMaxHorizontalRecoil();

	float GetSpread();

	float GetMinSpread();

	float GetMaxSpread();

	float GetSettleDelay();

	float GetSettleSpeed();

	bool GetIsReloading();

	float GetEquipTime();

	float GetUnequipTime();
	
	UFUNCTION(BlueprintCallable)
	ABaseCharacter* GetWielder();

	UAnimMontage* GetFPEquipAnimation();

	UAnimMontage* GetFPUnequipAnimation();

	UAnimMontage* GetTPEquipAnimation();

	UAnimMontage* GetTPUnequipAnimation();

	UPROPERTY(ReplicatedUsing=OnRep_RepVisible)
	bool RepVisible;

protected:
	void BeginPlay() override;

	void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintImplementableEvent)
		void OnFire();

	UFUNCTION(BlueprintImplementableEvent)
		void OnReloadStarted();

	UFUNCTION(BlueprintImplementableEvent)
		void OnReloadFinished();

	UFUNCTION(Server, Reliable, WithValidation)
		void ServerNotifyShot(FVector TraceStart, FVector TraceEnd, FHitResult HitResult);

	UFUNCTION(Server, Reliable, WithValidation)
		void ServerNotifyReload();

	UFUNCTION(NetMulticast, Unreliable)
		void MulticastNotifyReload();

	void ProcessShotConfirmed(FHitResult HitResult);

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		USkeletalMeshComponent* FPSkeletalMeshComp;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		USkeletalMeshComponent* TPSkeletalMeshComp;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Setup")
		EWeaponAnimationType WeaponAnimationType;

	/* Rapid fire allows players to shoot faster than the specified fire rate by tapping the fire button */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Setup")
		EFireStyle FireStyle;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Setup")
		float RPM;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Setup")
		float Damage;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Setup")
		TSubclassOf<UDamageType> DamageType;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Setup")
		float Range;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Setup")
		float MinVerticalRecoil;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Setup")
		float MaxVerticalRecoil;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Setup")
		float MinHorizontalRecoil;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Setup")
		float MaxHorizontalRecoil;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Setup")
		float Spread;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Setup")
		float MinSpread;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Setup")
		float MaxSpread;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Setup")
		float SettleDelay;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Setup")
		float SettleSpeed;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Setup")
		int32 MagazineCapacity;

	UPROPERTY(BlueprintReadOnly)
		int32 CurrentMagazineRounds;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Setup")
		float ReloadTime;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Setup")
		float UnequipTime;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Setup")
		float EquipTime;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Setup")
		TSubclassOf<class UUserWidget> WeaponHUDWidget;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Setup")
		TSubclassOf<class UUserWidget> CrosshairWidget;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Setup")
		UAnimMontage* FPFireAnimation;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Setup")
		UAnimMontage* TPHipFireAnimation;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Setup")
		UAnimMontage* TPADSFireAnimation;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Setup")
		UAnimMontage* FPReloadAnimation;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Setup")
		UAnimMontage* TPReloadAnimation;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Setup")
		UAnimMontage* FPEquipAnimation;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Setup")
		UAnimMontage* FPUnequipAnimation;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Setup")
		UAnimMontage* TPEquipAnimation;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Setup")
		UAnimMontage* TPUnequipAnimation;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Setup")
		TSubclassOf<AActor> BulletEffectClass;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Setup")
		USoundCue* FireSound;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Setup")
		USoundCue* RemoveMagazineSound;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Setup")
		USoundCue* InsertMagazineSound;

private:
	void Fire();

	void ApplyBulletDamage(FVector &OutBulletEndPoint, FHitResult &OutHitResult);

	void PlayFPEffects(FVector BulletEndPoint, FHitResult HitResult);

	void PlayTPEffects();

	bool IsTimeToFireAgain();

	FTimerHandle ReloadTimer;

	UFUNCTION()
	void ReloadFinished();

	UFUNCTION()
	void OnRep_LastShotInfo();

	UFUNCTION()
	void OnRep_RepVisible();

	UPROPERTY(Replicated)
	ABaseCharacter* Wielder;

	UPROPERTY(Replicated)
	UAimingComponent* CurrentAimingComp;

	bool IsTriggerDown;

	/** Trigger status of the previous frame */
	bool WasTriggerDown;

	bool IsReloading;

	bool CanFireSemiAuto;

	float LastShotTime;

	float FireRate;

	bool JustShotLastRound;

	UPROPERTY(ReplicatedUsing=OnRep_LastShotInfo)
	FRaycastShot LastShotInfo;
};
