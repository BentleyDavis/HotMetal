// Fill out your copyright notice in the Description page of Project Settings.

#include "PersonalWeapon.h"
#include "Components/SkeletalMeshComponent.h"
#include "Characters/BaseCharacter.h"
#include "Components/AimingComponent.h"
#include "Camera/CameraComponent.h"
#include "Engine/World.h"
#include "TimerManager.h"
#include "UnrealNetwork.h"
#include "HotMetal.h"
#include "Kismet/GameplayStatics.h"
#include "Animation/AnimInstance.h"
#include "Components/HealthComponent.h"
#include "PhysicalMaterials/PhysicalMaterial.h"
#include "BasePhysicalMaterial.h"
#include "Sound/SoundCue.h"

APersonalWeapon::APersonalWeapon()
{
	SetReplicates(true);
	PrimaryActorTick.bCanEverTick = true;

	// Initialize Defaults
	IsTriggerDown = false;
	WasTriggerDown = false;
	CanFireSemiAuto = true;
	LastShotTime = 0;
	FireStyle = EFireStyle::FULL_AUTO;
	RPM = 60.0f;
	Damage = 10.0f;
	Range = 100000.0f;
	MinVerticalRecoil = 0.1f;
	MaxVerticalRecoil = 0.5f;
	MinHorizontalRecoil = -0.1f;
	MaxHorizontalRecoil = 0.5f;
	Spread = 2.0f;
	MinSpread = 1.0f;
	MaxSpread = 10.0f;
	SettleDelay = 1.0;
	SettleSpeed = 3.0f;
	UnequipTime = 0.25f;
	EquipTime = 0.7f;

	SetRootComponent(CreateDefaultSubobject<USceneComponent>("RootComp"));

	FPSkeletalMeshComp = CreateDefaultSubobject<USkeletalMeshComponent>("FPSkeletalMeshComp");
	FPSkeletalMeshComp->SetupAttachment(RootComponent);
	FPSkeletalMeshComp->bOnlyOwnerSee = true;
	FPSkeletalMeshComp->SetIsReplicated(true);

	TPSkeletalMeshComp = CreateDefaultSubobject<USkeletalMeshComponent>("TPSkeletalMeshComp");
	TPSkeletalMeshComp->SetupAttachment(RootComponent);
	TPSkeletalMeshComp->bOwnerNoSee = true;
	TPSkeletalMeshComp->SetIsReplicated(true);
}

void APersonalWeapon::BeginPlay()
{
	Super::BeginPlay();

	FireRate = 60.0f / RPM;
	CurrentMagazineRounds = MagazineCapacity;
}

void APersonalWeapon::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (!IsTriggerDown)
	{
		CanFireSemiAuto = true;
	}


	if (IsTriggerDown)
	{
		if (CurrentMagazineRounds > 0 || JustShotLastRound)
		{
			switch (FireStyle)
			{
			case EFireStyle::SEMI_AUTO:
				if (CanFireSemiAuto && IsTimeToFireAgain())
				{
					Fire();
				}
				break;
			case EFireStyle::SEMI_AUTO_RAPID:
				if (!WasTriggerDown)
				{
					Fire();
				}
				break;
			case EFireStyle::FULL_AUTO:
				if (IsTimeToFireAgain())
				{
					Fire();
				}
				break;
			case EFireStyle::FULL_AUTO_RAPID:
				if (!WasTriggerDown)
				{
					Fire();
				}
				else if (IsTimeToFireAgain())
				{
					Fire();
				}
				break;
			default:
				UE_LOG(LogTemp, Error, TEXT("FireStyle not supported!"));
				break;
			}
		}
		else if (!WasTriggerDown)
		{
			Fire();
		}
	}

	WasTriggerDown = IsTriggerDown;
}

void APersonalWeapon::PressTrigger()
{
	if (!IsReloading)
	{
		IsTriggerDown = true;
	}
}

void APersonalWeapon::ReleaseTrigger()
{
	IsTriggerDown = false;
}

void APersonalWeapon::StartReload()
{
	if (!Wielder)
	{
		UE_LOG(LogTemp, Error, TEXT("Weapon has no valid Wielder!"));
		return;
	}

	if (!IsReloading && CurrentMagazineRounds != MagazineCapacity)
	{
		IsTriggerDown = false;
		IsReloading = true;
		Wielder->SetIsADS(false);
		GetWorldTimerManager().SetTimer(ReloadTimer, this, &APersonalWeapon::ReloadFinished, ReloadTime);

		// Play reload animation
		if (FPReloadAnimation)
		{
			float PlayRate = FPReloadAnimation->GetPlayLength() / ReloadTime;
			Wielder->GetMesh1P()->GetAnimInstance()->Montage_Play(FPReloadAnimation, PlayRate);
		}
		else
		{
			UE_LOG(LogTemp, Warning, TEXT("Weapon has no valid FPReloadAnimation"));
		}

		OnReloadStarted();

		ServerNotifyReload();
	}
}

void APersonalWeapon::ReloadFinished()
{
	CurrentMagazineRounds = MagazineCapacity;
	IsReloading = false;
	OnReloadFinished();
}

void APersonalWeapon::ServerNotifyReload_Implementation()
{
	MulticastNotifyReload();
}

bool APersonalWeapon::ServerNotifyReload_Validate()
{
	return true;
}

void APersonalWeapon::MulticastNotifyReload_Implementation()
{
	if (!Wielder)
	{
		UE_LOG(LogTemp, Error, TEXT("Weapon has no valid Wielder!"));
		return;
	}

	if (!Wielder->IsLocallyControlled())
	{
		UE_LOG(LogTemp, Warning, TEXT("Multicast called"));
		// Play reload animation
		if (TPReloadAnimation)
		{
			float PlayRate = TPReloadAnimation->GetPlayLength() / ReloadTime;
			Wielder->GetMesh3P()->GetAnimInstance()->Montage_Play(TPReloadAnimation, PlayRate);
		}
		else
		{
			UE_LOG(LogTemp, Warning, TEXT("Weapon has no valid TPReloadAnimation"));
		}
	}
}

void APersonalWeapon::SetupWielder(ABaseCharacter* Wielder)
{
	this->Wielder = Wielder;

	if (!Wielder)
	{
		UE_LOG(LogTemp, Error, TEXT("%s could not attach to owner because Wielder is null!"), *(GetName()));
		return;
	}

	CurrentAimingComp = Wielder->GetAimingComponent();

	if (!CurrentAimingComp)
	{
		UE_LOG(LogTemp, Error, TEXT("Wielder on PersonalWeapon does not have a valid AimingComponent!"));
		return;
	}

	if (!(FPSkeletalMeshComp && TPSkeletalMeshComp))
	{
		UE_LOG(LogTemp, Error, TEXT("%s could not attach to owner because either FPSkeletalMeshComp or TPSkeletalMeshComp is not assigned!"), *(GetName()));
	}

	USkeletalMeshComponent* PawnMesh1p = Wielder->GetMesh1P();
	USkeletalMeshComponent* PawnMesh3p = Wielder->GetMesh3P();
	FPSkeletalMeshComp->AttachToComponent(PawnMesh1p, FAttachmentTransformRules(EAttachmentRule::SnapToTarget, true), FName("GripPoint"));
	TPSkeletalMeshComp->AttachToComponent(PawnMesh3p, FAttachmentTransformRules(EAttachmentRule::SnapToTarget, true), FName("GripPoint"));
}

USkeletalMeshComponent* APersonalWeapon::GetFPSkeletalMeshComp()
{
	return FPSkeletalMeshComp;
}

USkeletalMeshComponent* APersonalWeapon::GetTPSkeletalMeshComp()
{
	return TPSkeletalMeshComp;
}

TSubclassOf<UUserWidget> APersonalWeapon::GetWeaponHUDWidget()
{
	return WeaponHUDWidget;
}

float APersonalWeapon::GetMinVerticalRecoil()
{
	return MinVerticalRecoil;
}

float APersonalWeapon::GetMaxVerticalRecoil()
{
	return MaxVerticalRecoil;
}

float APersonalWeapon::GetMinHorizontalRecoil()
{
	return MinHorizontalRecoil;
}

float APersonalWeapon::GetMaxHorizontalRecoil()
{
	return MaxHorizontalRecoil;
}

float APersonalWeapon::GetSpread()
{
	return Spread;
}

float APersonalWeapon::GetMinSpread()
{
	return MinSpread;
}

float APersonalWeapon::GetMaxSpread()
{
	return MaxSpread;
}

float APersonalWeapon::GetSettleDelay()
{
	return SettleDelay;
}

float APersonalWeapon::GetSettleSpeed()
{
	return SettleSpeed;
}

bool APersonalWeapon::GetIsReloading()
{
	return IsReloading;
}

float APersonalWeapon::GetEquipTime()
{
	return EquipTime;
}

float APersonalWeapon::GetUnequipTime()
{
	return UnequipTime;
}

ABaseCharacter* APersonalWeapon::GetWielder()
{
	return Wielder;
}

UAnimMontage* APersonalWeapon::GetFPEquipAnimation()
{
	return FPEquipAnimation;
}

UAnimMontage* APersonalWeapon::GetFPUnequipAnimation()
{
	return FPUnequipAnimation;
}

UAnimMontage* APersonalWeapon::GetTPEquipAnimation()
{
	return TPEquipAnimation;
}

UAnimMontage* APersonalWeapon::GetTPUnequipAnimation()
{
	return TPUnequipAnimation;
}

void APersonalWeapon::Fire()
{
	if (JustShotLastRound)
	{
		JustShotLastRound = false;
	}

	if (CurrentMagazineRounds > 0)
	{
		if (!CurrentAimingComp)
		{
			UE_LOG(LogTemp, Error, TEXT("PersonalWeapon does not have a reference to a valid AimingComponent!"));
			return;
		}

		// Valid shot; apply damage and effects
		FVector FireDirection;
		FHitResult HitResult;
		ApplyBulletDamage(FireDirection, HitResult);
		PlayFPEffects(FireDirection, HitResult);
		CurrentAimingComp->ApplyRecoil();

		LastShotTime = GetWorld()->GetTimeSeconds();
		CanFireSemiAuto = false;
		CurrentMagazineRounds--;

		if (CurrentMagazineRounds == 0)
		{
			JustShotLastRound = true;
		}

		OnFire();
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("Play empty clip sound"));
	}
}

void APersonalWeapon::ApplyBulletDamage(FVector &OutBulletEndPoint, FHitResult& OutHitResult)
{
	if (!Wielder)
	{
		UE_LOG(LogTemp, Error, TEXT("Weapon has no valid Wielder!"));
		return;
	}

	FVector FireDirection = CurrentAimingComp->GetFiringVector();
	FVector BulletStartPoint = Wielder->GetFirstPersonCameraComponent()->GetComponentLocation();
	OutBulletEndPoint = BulletStartPoint + FireDirection * Range;
	FCollisionQueryParams Params = FCollisionQueryParams(SCENE_QUERY_STAT(WeaponTrace), true, Wielder);
	Params.bReturnPhysicalMaterial = true;

	GetWorld()->LineTraceSingleByChannel(OutHitResult, BulletStartPoint, OutBulletEndPoint, COLLISION_WEAPON, Params);

	if (OutHitResult.GetActor())
	{
		if (!DamageType)
		{
			UE_LOG(LogTemp, Error, TEXT("Invalid DamageType!"));
			return;
		}

		if (OutHitResult.GetActor()->GetComponentByClass(UHealthComponent::StaticClass()))
		{
			Wielder->OnHitEnemy();
		}
	}

	ServerNotifyShot(BulletStartPoint, OutBulletEndPoint, OutHitResult);
}

void APersonalWeapon::ServerNotifyShot_Implementation(FVector TraceStart, FVector TraceEnd, FHitResult HitResult)
{
	FRaycastShot RaycastShot;
	RaycastShot.LastShotTime = GetWorld()->GetTimeSeconds();
	RaycastShot.TraceFrom = TraceStart;
	RaycastShot.TraceTo = TraceEnd;
	LastShotInfo = RaycastShot;

	ProcessShotConfirmed(HitResult);
}

bool APersonalWeapon::ServerNotifyShot_Validate(FVector TraceStart, FVector TraceEnd, FHitResult HitResult)
{
	return true;
}

void APersonalWeapon::ProcessShotConfirmed(FHitResult HitResult)
{
	if (Role == ROLE_Authority)
	{
		FVector FireDirection = Wielder->GetFirstPersonCameraComponent()->GetComponentRotation().Vector();
		UGameplayStatics::ApplyPointDamage(HitResult.GetActor(), Damage, FireDirection, HitResult, Wielder->GetController(), this, DamageType);
	}
}

void APersonalWeapon::PlayFPEffects(FVector BulletEndPoint, FHitResult HitResult)
{
	if (!Wielder)
	{
		UE_LOG(LogTemp, Error, TEXT("Weapon has no valid Wielder!"));
		return;
	}

	// Animation effect
	if (!Wielder->GetIsADS()) // Don't worry about FP animations when ADS, recoil is handled procedurally
	{
		if (FPFireAnimation)
		{
			Wielder->GetMesh1P()->GetAnimInstance()->Montage_Play(FPFireAnimation);
		}
		else
		{
			UE_LOG(LogTemp, Warning, TEXT("Weapon has no valid FPFireAnimation"));
		}
	}

	// Fire sound effect
	if (FireSound)
	{
		UGameplayStatics::PlaySoundAtLocation(GetWorld(), FireSound, GetFPSkeletalMeshComp()->GetSocketLocation(FName("Muzzle")));
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("%s has no valid FireSound"), *(GetName()));
	}

	// Bullet trace effect
	if (BulletEffectClass)
	{
		FActorSpawnParameters SpawnParams = FActorSpawnParameters();
		SpawnParams.Instigator = Wielder;
		FRotator BulletRotation = (BulletEndPoint - GetFPSkeletalMeshComp()->GetSocketLocation(FName("Muzzle"))).Rotation();
		AActor* SpawnedActor = GetWorld()->SpawnActor<AActor>(BulletEffectClass, GetFPSkeletalMeshComp()->GetSocketLocation(FName("Muzzle")), BulletRotation, SpawnParams);
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("BulletEffectClass is null!"));
	}

	// Impact effect
	UPhysicalMaterial* PhysicalMaterial = HitResult.PhysMaterial.Get();
	UBasePhysicalMaterial* BasePhysicalMaterial = Cast<UBasePhysicalMaterial>(PhysicalMaterial);

	if (BasePhysicalMaterial)
	{
		// Play impact particle effect
		UParticleSystem* HitFX = BasePhysicalMaterial->GetBulletImpactParticleFX();

		FVector HitFXLocation = HitResult.Location + (HitResult.Normal * 10.0f);
		if (HitFX)
		{
			UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), HitFX, HitFXLocation, HitResult.Normal.Rotation());
		}
		else
		{
			UE_LOG(LogTemp, Error, TEXT("%s does not have a BulletImpactParticleFX assigned!"), *(BasePhysicalMaterial->GetName()));
		}

		// Play impact sound effect
		USoundCue* SoundFX = BasePhysicalMaterial->GetBulletImpactSoundFX();

		if (SoundFX)
		{
			UGameplayStatics::PlaySoundAtLocation(GetWorld(), SoundFX, HitFXLocation);
		}
		else
		{
			UE_LOG(LogTemp, Error, TEXT("%s does not have a BulletImpactSoundFX assigned!"), *(BasePhysicalMaterial->GetName()));
		}
	}
	else
	{
		if (PhysicalMaterial)
		{
			UE_LOG(LogTemp, Error, TEXT("%s's PhysicalMaterial is not of type UBasePhysicalMaterial; No particles or sounds will be played!"), *(PhysicalMaterial->GetName()));
		}
	}
}

void APersonalWeapon::PlayTPEffects()
{
	if (!Wielder)
	{
		UE_LOG(LogTemp, Error, TEXT("Weapon has no valid Wielder!"));
		return;
	}

	// Effects have already been played on the owning client
	if (!Wielder->IsLocallyControlled())
	{
		// Animation effect
		if (Wielder->GetIsADS())
		{
			if (TPADSFireAnimation)
			{
				Wielder->GetMesh3P()->GetAnimInstance()->Montage_Play(TPADSFireAnimation);
			}
			else
			{
				UE_LOG(LogTemp, Warning, TEXT("Weapon has no valid TPADSFireAnimation"));
			}
		}
		else
		{
			if (TPHipFireAnimation)
			{
				Wielder->GetMesh3P()->GetAnimInstance()->Montage_Play(TPHipFireAnimation);
			}
			else
			{
				UE_LOG(LogTemp, Warning, TEXT("Weapon has no valid TPHipFireAnimation"));
			}
		}

		// Fire sound effect
		if (FireSound)
		{
			UGameplayStatics::PlaySoundAtLocation(GetWorld(), FireSound, GetTPSkeletalMeshComp()->GetSocketLocation(FName("Muzzle")));
		}
		else
		{
			UE_LOG(LogTemp, Warning, TEXT("%s has no valid FireSound"), *(GetName()));
		}

		// Bullet trace effect
		if (BulletEffectClass)
		{
			FActorSpawnParameters SpawnParams = FActorSpawnParameters();
			SpawnParams.Instigator = Wielder;
			FRotator BulletRotation = (LastShotInfo.TraceTo - GetTPSkeletalMeshComp()->GetSocketLocation(FName("Muzzle"))).Rotation();
			AActor * SpawnedActor = GetWorld()->SpawnActor<AActor>(BulletEffectClass, GetTPSkeletalMeshComp()->GetSocketLocation(FName("Muzzle")), BulletRotation, SpawnParams);
		}
		else
		{
			UE_LOG(LogTemp, Warning, TEXT("BulletEffectClass is null!"));
		}

		// Impact effect
		FHitResult HitResult;
		FCollisionQueryParams Params = FCollisionQueryParams(SCENE_QUERY_STAT(WeaponTrace), true, Wielder);
		Params.bReturnPhysicalMaterial = true;

		GetWorld()->LineTraceSingleByChannel(HitResult, LastShotInfo.TraceFrom, LastShotInfo.TraceTo, COLLISION_WEAPON, Params);
		UPhysicalMaterial* PhysicalMaterial = HitResult.PhysMaterial.Get();
		UBasePhysicalMaterial* BasePhysicalMaterial = Cast<UBasePhysicalMaterial>(PhysicalMaterial);

		if (BasePhysicalMaterial)
		{
			// Play impact particle effect
			UParticleSystem* HitFX = BasePhysicalMaterial->GetBulletImpactParticleFX();

			FVector HitFXLocation = HitResult.Location + (HitResult.Normal);
			if (HitFX)
			{
				UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), HitFX, HitFXLocation, HitResult.Normal.Rotation());
			}
			else
			{
				UE_LOG(LogTemp, Error, TEXT("%s does not have a HitFX assigned!"), *(BasePhysicalMaterial->GetName()));
			}

			// Play impact sound effect
			USoundCue* SoundFX = BasePhysicalMaterial->GetBulletImpactSoundFX();

			if (SoundFX)
			{
				UGameplayStatics::PlaySoundAtLocation(GetWorld(), SoundFX, HitFXLocation);
			}
			else
			{
				UE_LOG(LogTemp, Error, TEXT("%s does not have a BulletImpactSoundFX assigned!"), *(BasePhysicalMaterial->GetName()));
			}
		}
		else
		{
			if (PhysicalMaterial)
			{
				UE_LOG(LogTemp, Error, TEXT("%s's PhysicalMaterial is not of type UBasePhysicalMaterial; No particles or sounds will be played!"), *(PhysicalMaterial->GetName()));
			}
		}
	}
}

bool APersonalWeapon::IsTimeToFireAgain()
{
	return GetWorld()->GetTimeSeconds() - LastShotTime > FireRate;
}

#pragma region Replication

void APersonalWeapon::OnRep_LastShotInfo()
{
	PlayTPEffects();
}

void APersonalWeapon::OnRep_RepVisible()
{
	GetFPSkeletalMeshComp()->SetVisibility(RepVisible);
	GetTPSkeletalMeshComp()->SetVisibility(RepVisible);
}

void APersonalWeapon::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(APersonalWeapon, Wielder);
	DOREPLIFETIME(APersonalWeapon, CurrentAimingComp);
	DOREPLIFETIME(APersonalWeapon, LastShotInfo);
	DOREPLIFETIME(APersonalWeapon, RepVisible);
}

#pragma endregion
