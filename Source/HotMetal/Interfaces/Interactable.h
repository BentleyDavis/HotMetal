// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "Interactable.generated.h"

class AActor;

// This class does not need to be modified.
UINTERFACE(MinimalAPI)
class UInteractable : public UInterface
{
	GENERATED_BODY()
};

/**
 * 
 */
class HOTMETAL_API IInteractable
{
	GENERATED_BODY()

public:
	virtual void StartFocus() = 0;

	virtual void StopFocus() = 0;

	virtual void OnInteract(AActor* Instigator) = 0;
};
